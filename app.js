
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');


const config = require('./config/config');


mongoose.set('useCreateIndex', true);

const models = require("./models");

const app = express();


const PORT = process.env.PORT || 3000;


app.use(cors());


app.use(express.static(path.join(__dirname, 'public')));


app.use(bodyParser.json());


app.get('/', (req, res) => {
    return res.json({
        message: "E-Voting Application"
    });
});


const evoting = require('./routes/v1');
app.use('/v1', evoting);

app.listen(PORT, () => {
    console.log('Server started on port',PORT);
});

to = function(promise) {
    return promise
    .then(data => {
        return [null, data];
    }).catch(err =>
        [err]
    );
}